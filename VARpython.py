import yfinance as yf
import pandas as pd
import numpy as np
from statsmodels.tsa.vector_ar.var_model import VAR
from sklearn.metrics import mean_squared_error

# define the list of tickers to download historical data for
tickers = ["AAPL", "GOOG", "MSFT", "AMZN", "FB"]

# download historical stock price data for all tickers

data = yf.download(tickers,start="2019-01-01",end_date = "2022-12-31")

# create a DataFrame of closing prices for all tickers
close_prices = pd.concat([data[ticker]['Close'] for ticker in tickers], axis=1, keys=tickers)

# split the data into training and testing sets
train_size = int(len(close_prices) * 0.8)
train_data = close_prices.iloc[:train_size]
test_data = close_prices.iloc[train_size:]

# fit a VAR model to the training data
model = VAR(train_data)
results = model.fit(maxlags=15, ic='aic')

# make predictions on the test data
predictions = results.forecast(test_data.values, len(test_data))

# calculate the root mean squared error (RMSE) of the predictions
rmse = {}
for i, ticker in enumerate(tickers):
    rmse[ticker] = np.sqrt(mean_squared_error(test_data[ticker], predictions[:, i]))
    print(f"Root mean squared error for {ticker}: {rmse[ticker]:.2f}")
